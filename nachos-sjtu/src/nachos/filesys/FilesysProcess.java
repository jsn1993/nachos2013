package nachos.filesys;

import java.util.Arrays;

import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.vm.VMProcess;

/**
 * FilesysProcess is used to handle syscall and exception through some callback methods.
 * 
 * @author starforever
 */
public class FilesysProcess extends VMProcess
{
	protected static final int SYSCALL_MKDIR = 14;
	protected static final int SYSCALL_RMDIR = 15;
	protected static final int SYSCALL_CHDIR = 16;
	protected static final int SYSCALL_GETCWD = 17;
	protected static final int SYSCALL_READDIR = 18;
	protected static final int SYSCALL_STAT = 19;
	protected static final int SYSCALL_LINK = 20;
	protected static final int SYSCALL_SYMLINK = 21;
	
	private RealFileSystem fileSystem()
	{
		return FilesysKernel.realFileSystem;
	}
	protected int handleMkdir(int a0)
	{
		if (fileSystem().createFolder(readVirtualMemoryString(a0,256)))	return 0;
		return -1;
	}
	protected int handleRmdir(int a0)
	{
		if (fileSystem().removeFolder(readVirtualMemoryString(a0,256)))	return 0;
		return -1;
	}
	protected int handleChdir(int a0)
	{
		if (fileSystem().changeCurFolder(readVirtualMemoryString(a0,256)))	return 0;
		return -1;
	}
	protected int handleGetcwd(int a0,int a1)
	{
		String cwd=fileSystem().cwdToString();
		if (cwd.length()>a1)	return -1;
		
		byte[] buf=new byte[a1];
		Arrays.fill(buf,(byte)0);
		System.arraycopy(cwd.getBytes(),0,buf,0,cwd.length());
		
		writeVirtualMemory(a0,buf);
		return -1;
	}
	protected int handleReaddir(int a0,int a1,int a2,int a3)
	{
		String[] ret=fileSystem().readDir(readVirtualMemoryString(a0,256));// ret[a2][a3]
		if (ret==null)	return -1;// dirname doesn't exist
		for (int i=0;i<ret.length;++i)
		if (ret[i].length()>a3)// entry name longer than namesize
			return -1;
		if (ret.length>a2)	return -1;// number of entries bigger than size
		
		byte[] buf=new byte[a2*a3];
		Arrays.fill(buf,(byte)0);
		for (int i=0;i<ret.length;++i)
		{
			byte[] tmp=ret[i].getBytes();
			System.arraycopy(tmp,0,buf,i*a3,tmp.length);
		}
		
		writeVirtualMemory(a1,buf);
		return ret.length;
	}
	protected int handleStat(int a0,int a1)
	{
		FileStat fs=fileSystem().getStat(readVirtualMemoryString(a0,256));
		if (fs==null)	return -1;
		
		byte[] buf=new byte[276];
		Arrays.fill(buf,(byte)0);
		System.arraycopy(fs.name.getBytes(),0,buf,0,fs.name.length());
		File.I2B(fs.size,buf,256);
		File.I2B(fs.sectors,buf,260);
		File.I2B(fs.type,buf,264);
		File.I2B(fs.inode,buf,268);
		File.I2B(fs.links,buf,272);
		
		writeVirtualMemory(a1,buf);
		return 0;
	}
	protected int handleLink(int a0,int a1)
	{
		if (fileSystem().createLink(readVirtualMemoryString(a0,256),readVirtualMemoryString(a1,256)))
			return 0;
		return -1;
	}
	protected int handleSymlink(int a0,int a1)
	{
		if (fileSystem().createSymlink(readVirtualMemoryString(a0,256),readVirtualMemoryString(a1,256)))
			return 0;
		return -1;
	}

	protected static final String[] syscallName={
		"Halt","Exit","Exec","Join","Create","Open","Read","Write","Close","Unlink",
		"Mmap","Connect","Accept","Getpid","Mkdir","Rmdir","Chdir","Getcwd","Readdir","Stat",
		"Link","Symlink"};
	
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3)
	{
		if (RealFileSystem.debug)System.out.println(syscallName[syscall]);
		switch (syscall)
		{
			case SYSCALL_MKDIR:
				return handleMkdir(a0);
			case SYSCALL_RMDIR:
				return handleRmdir(a0);
			case SYSCALL_CHDIR:
				return handleChdir(a0);
			case SYSCALL_GETCWD:
				return handleGetcwd(a0,a1);
			case SYSCALL_READDIR:
				return handleReaddir(a0,a1,a2,a3);
			case SYSCALL_STAT:
				return handleStat(a0,a1);
			case SYSCALL_LINK:
				return handleLink(a0,a1);
			case SYSCALL_SYMLINK:
				return handleSymlink(a0,a1);
			default:
				return super.handleSyscall(syscall, a0, a1, a2, a3);
		}
	}
	
	public void handleException(int cause)
	{
		if (cause == Processor.exceptionSyscall) {
			Processor processor = Machine.processor();
			int result = handleSyscall(processor.readRegister(Processor.regV0),
					processor.readRegister(Processor.regA0), processor
							.readRegister(Processor.regA1), processor
							.readRegister(Processor.regA2), processor
							.readRegister(Processor.regA3));
			processor.writeRegister(Processor.regV0, result);
			processor.advancePC();
		}
		else
			super.handleException(cause);
	}
}

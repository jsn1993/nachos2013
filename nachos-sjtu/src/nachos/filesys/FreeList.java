package nachos.filesys;

import java.util.Arrays;
import java.util.*;
import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.filesys.INode;

/**
 * FreeList is a single special file used to manage free space of the filesystem.
 * It maintains a list of sector numbers to indicate those that are available to use.
 * When there's a need to allocate a new sector in the filesystem, call allocate().
 * And you should call deallocate() to free space at a appropriate time(eg. when a file is deleted) for reuse in the future.
 * 
 * @author starforever
 */
public class FreeList extends File
{
	/** the static address */
	public static int STATIC_ADDR = 0;
	
	/** size occupied in the disk(bitmap) */
	static int size = Lib.divRoundUp(Disk.NumSectors, 8);
	
	private static int numSectors=Disk.NumSectors;
	/** maintain address of all the free sectors */
	private LinkedList<Integer> free_list;
	
	public FreeList(INode inode)
	{
		super(inode);
		free_list = new LinkedList<Integer>();
	}
	
	public void init()
	{
		for(int i = 2; i < Disk.NumSectors; ++i)
			free_list.add(i);
		save();
	}
	
	public boolean isFree(int addr)
	{
		return free_list.contains(addr);
	}
	/** allocate a new sector in the disk */
	public int allocate()
	{
		int free=free_list.removeFirst();
//		System.out.println("take "+free);
		return free;
	}
	
	/** deallocate a sector to be reused */
	public void deallocate(int sec)
	{
		free_list.add(sec);
//		System.out.println("return "+sec);
	}
	
	/** save the content of freelist to the disk */
	// write all sec in order to read once
	public void save()
	{
		byte[] buf=new byte[size];
		Arrays.fill(buf,(byte)0);
		
		for (Iterator<Integer> it=free_list.iterator();it.hasNext();)
		{
			int freeSec=it.next();
			buf[freeSec>>3]|=(1<<(freeSec%8));
		}
		write(0,buf,0,size);
	}
	
	/** load the content of freelist from the disk */
	// only read once 
	public void load()
	{
		byte[] buf=new byte[size];
		read(0,buf,0,size);
		free_list.clear();
		
		for (int i=0;i<numSectors;++i)
		if ((buf[i>>3]&(1<<(i%8)))!=0)
			free_list.add(i);
	}
}

package nachos.filesys;

import nachos.machine.Disk;
import nachos.machine.Machine;
import nachos.machine.OpenFile;

/**
 * File provide some basic IO operations.
 * Each File is associated with an INode which stores the basic information for the file.
 * 
 * @author starforever
 */
public class File extends OpenFile
{
	INode inode;
	
	private int pos;
	
	private int sectorSize;
	
	public File(INode inode)
	{
		this.inode = inode;
		pos = 0;
		sectorSize=Disk.SectorSize;
	}
	
	public int length()
	{
		return inode.file_size;
	}
	
	public void close()
	{
		--inode.use_count;
		if (inode.use_count==0 && inode.link_count==0)
			inode.free();
		inode=null;
	}
	
	public void seek(int pos)
	{
		this.pos = pos;
	}
	
	public int tell()
	{
		return pos;
	}
	
	public int read(byte[] buffer, int start, int limit)
	{
		int ret = read(pos, buffer, start, limit);
		pos += ret;
		return ret;
	}
	
	public int write(byte[] buffer, int start, int limit)
	{
		int ret = write(pos, buffer, start, limit);
		pos += ret;
		return ret;
	}
	
	private int secnum(int pos)
	{
		return pos/Disk.SectorSize;
	}
	private int secofs(int pos)
	{
		return pos%sectorSize;
	}
	private int calcpos(int secn,int seco)
	{
		return secn*sectorSize+seco;
	}
	public int read(int pos, byte[] buffer, int start, int limit)
	{
		inode.acquire();
		
		int file_size=length();
		if (pos>=file_size)
		{
			inode.release();
			return -1;
		}
		
		int amount=0,l=Math.min(limit,file_size-pos),o=start;
		byte[] readbuf=new byte[sectorSize];
		do
		{
			int psec=inode.getSector(secnum(pos));
			if (psec<0)
			{
				inode.release();
				return amount;
			}
			int inThisSector=Math.min(l, sectorSize-secofs(pos));
			Machine.synchDisk().readSector(psec,readbuf,0);
			System.arraycopy(readbuf,secofs(pos),buffer,o,inThisSector);
			
			l-=inThisSector;
			amount+=inThisSector;
			o+=inThisSector;
			pos=calcpos(secnum(pos)+1,0);
		}
		while (l>0);
		
		inode.release();
		return amount;
	}
	
	public int write(int pos, byte[] buffer, int start, int limit)
	{
		inode.acquire();
		if (pos+limit>=inode.file_size)
			inode.setFileSize(pos+limit);

		byte[] writebuf=new byte[sectorSize];
		int amount=0,l=limit,o=start;
		do
		{
			int psec=inode.getSector(secnum(pos));
			int inThisSector=Math.min(l,sectorSize-secofs(pos));
			
			Machine.synchDisk().readSector(psec,writebuf,0);
			System.arraycopy(buffer,o,writebuf,secofs(pos),inThisSector);
			Machine.synchDisk().writeSector(psec,writebuf,0);
			
			l-=inThisSector;
			amount+=inThisSector;
			o+=inThisSector;
			pos=calcpos(secnum(pos)+1,0);
		}
		while (l>0);
		inode.release();
		return amount;
	}
	
	// util

	public static void I2B(int val,byte[] buf,int pos)
	{
		buf[pos+3]=(byte)(val>>24&0xff);
		buf[pos+2]=(byte)(val>>16&0xff);
		buf[pos+1]=(byte)(val>>8&0xff);
		buf[pos]=(byte)(val&0xff);
	}
	
	public static int B2I(byte[] buf, int pos)
	{
		return (buf[pos+3]<<24)|((buf[pos+2]<<16)&0xff0000)
			  |((buf[pos+1]<<8)&0xff00)|(buf[pos]&0xff);
	}
	
	protected RealFileSystem fileSystem()
	{
		return FilesysKernel.realFileSystem;
	}
}

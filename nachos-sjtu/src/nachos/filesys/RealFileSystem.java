package nachos.filesys;

import java.util.*;
import nachos.machine.FileSystem;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;

/**
 * RealFileSystem provide necessary methods for filesystem syscall.
 * The FileSystem interface already define two basic methods, you should implement your own to adapt to your task.
 * 
 * @author starforever
 */
public class RealFileSystem implements FileSystem
{
	public static boolean debug=false;
	/** the free list */
	private FreeList free_list;
	
	/** the root folder */
	private Folder root_folder;
	
	/** the string representation of the current folder */
	private LinkedList<String> cur_path = new LinkedList<String>();
	
	// to serve getcwd
	private LinkedList<Folder> cur_fold = new LinkedList<Folder>(); 
	

	private HashMap<Integer,INode> INodes = new HashMap<Integer,INode>();
	/**
	 * initialize the file system
	 * 
	 * @param format
	 *					whether to format the file system
	 */
	public void init(boolean format)
	{
		if (format)
		{
			INode inode_free_list = new INode(FreeList.STATIC_ADDR);
			INodes.put(FreeList.STATIC_ADDR,inode_free_list);
			free_list = new FreeList(inode_free_list);
			free_list.init();
			
			INode inode_root_folder = new INode(Folder.STATIC_ADDR);
			INodes.put(Folder.STATIC_ADDR,inode_root_folder);
			root_folder = new Folder(inode_root_folder);
			root_folder.save();
			cur_fold.add(root_folder);
			cur_path.add("");
			
			importStub();
		}
		else
		{
			INode inode_free_list = new INode(FreeList.STATIC_ADDR);
			inode_free_list.load();
			INodes.put(FreeList.STATIC_ADDR,inode_free_list);
			free_list = new FreeList(inode_free_list);
			free_list.load();
			
			INode inode_root_folder = new INode(Folder.STATIC_ADDR);
			inode_root_folder.load();
			INodes.put(Folder.STATIC_ADDR,inode_root_folder);
			root_folder = new Folder(inode_root_folder);
			root_folder.load();
			cur_fold.add(root_folder);
			cur_path.add("");
		}
	}
	
	public void finish()
	{
		root_folder.save();
		free_list.save();
		for (Iterator<INode> it=INodes.values().iterator();it.hasNext();)
			it.next().save();
	}
	
	/** import from stub filesystem */
	private void importStub()
	{
		FileSystem stubFS = Machine.stubFileSystem();
		FileSystem realFS = FilesysKernel.realFileSystem;
		String[] file_list = Machine.stubFileList();
		for (int i = 0; i < file_list.length; ++i)
		{
			if (!file_list[i].endsWith(".coff") && !file_list[i].equals("SWAP"))
				continue;
			OpenFile src = stubFS.open(file_list[i], false);
			if (src == null)
			{
				continue;
			}
			OpenFile dst = realFS.open(file_list[i], true);
			int size = src.length();
			if (size>0)
			{
				byte[] buffer = new byte[size];
				src.read(0, buffer, 0, size);
				dst.write(0, buffer, 0, size);
			}
			src.close();
			dst.close();
		}
	}
	
	/** get the only free list of the file system */
	public FreeList getFreeList()
	{
		return free_list;
	}
	
	/** get the only root folder of the file system */
	public Folder getRootFolder()
	{
		return root_folder;
	}
	
	public INode findINode(int addr)
	{
		INode ret=INodes.get(addr);
		if (ret!=null)	return ret;
		
		INodes.put(addr,ret=new INode(addr));
		//if (!free_list.isFree(addr))
		//	ret.load();
		
		return ret;
	}
	// query the string as path and store data temporarily
	private Folder Qfather;
	private int Qaddr;
	private String Qname;
	private LinkedList<String> Qpath;
	private LinkedList<Folder> Qfold;
	private boolean queryPath(String name) 
	{
		if (debug)System.out.println("filesys query: "+name);
		Qfather=null;
		Qname=null;
		Qpath=new LinkedList<String>();
		Qfold=new LinkedList<Folder>();
		// first depth
		if (name.charAt(0)=='/')
		{
			Qfold.add(root_folder);
			if (name.length()==1)
			{
				Qfather=null;// i'm god
				Qaddr=Folder.STATIC_ADDR;
				Qname="";
				return true;
			}
			name=name.substring(1);
		}
		else
		{
			Qfold.addAll(cur_fold);
			Qpath.addAll(cur_path);
		}
		// chain all ancestors
		String[] list=name.split("/");
		for (int i=0;i<list.length-1;++i)
		if (list[i].equals(".") || list[i].equals(""))	continue;
		else
		if (list[i].equals(".."))
		{
			Qfold.removeLast();
			Qpath.removeLast();
			if (Qfold.isEmpty())	return false;
		}
		else
		{
			Folder folder=Qfold.getLast();
			folder.load();
			FolderEntry e=folder.findEntry(list[i]);
			if (e==null)	return false;
			INode node=findINode(e.addr);
			if (node.file_type!=INode.TYPE_FOLDER)	return false;
			
			Qfold.add(new Folder(node));
			Qpath.add(list[i]);
		}
		// check itself
		String self=list[list.length-1];
		if (self.equals("..") && Qfold.size()<=1)	return false;
		
		// i'm an indicator
		if (self.equals("") || self.equals(".") || self.equals(".."))
		{
			if (self.equals(".."))
			{
				Qfold.removeLast();
				Qpath.removeLast();
			}
			if (Qfold.size()==1)
			{
				Qfather=null;// i'm god
				Qaddr=Folder.STATIC_ADDR;
				Qname="";
			}
			else
			{
				Folder folder=Qfold.removeLast();
				Qfather=Qfold.getLast();
				Qaddr=folder.inode.getAddr();
				Qname=Qpath.removeLast();
			}
		}
		else
		{
			Qfather=Qfold.getLast();
			if (Qfather.inode.file_type==INode.TYPE_FOLDER_DEL)	return false;	
			FolderEntry e=Qfather.findEntry(self);
			Qname=self;
			if (e==null)	Qaddr=-1;
			else	Qaddr=e.addr;
		}
		
		return true;
	}
	
	public OpenFile open(String name, boolean create)
	{
		if (debug)System.out.println("filesys open: "+name);
		if (!queryPath(name))	return null;
		if (create)
			if (Qaddr>=0)	return null;
			else
			{
				int addr=Qfather.create(Qname);
				INode node=findINode(addr);
				++node.use_count;
				return new File(node);
			}
		if (Qaddr>=0)
		{
			INode node=findINode(Qaddr);
			if (node.file_type==INode.TYPE_FILE)
			{
				++node.use_count;
				return new File(node);
			}
			else
			if (node.file_type==INode.TYPE_SYMLINK)
			{
				byte[] buf=new byte[1024];
				new File(node).read(0,buf,0,1024);
				if (name.equals("file1_link"))
				{
					int i=0;
					++i;
				}
				if (queryPath(Lib.bytesToString(buf,0,1024)) && Qaddr>=0)
				{
					INode linkto=findINode(Qaddr);
					if (linkto.file_type==INode.TYPE_FILE)
					{
						++linkto.use_count;
						return new File(linkto);
					}
				}
			}
		}
		
		return null;
	}
	
	public boolean remove(String name)
	{
		if (debug)System.out.println("filesys remove: "+name);
		if (!queryPath(name) || Qaddr<0)	return false;
		
		INode node=findINode(Qaddr);
		if (node.file_type==INode.TYPE_FILE || node.file_type==INode.TYPE_SYMLINK)
		{
			Qfather.removeEntry(Qname);
			if (node.link_count==0)
			{
				node.file_type=INode.TYPE_FILE_DEL;
				if (node.use_count==0)
				{
					INodes.remove(Qaddr);
					node.free();
				}
			}
			return true;
		}
		return false;
	}
	
	public boolean createFolder(String name)
	{
		if (debug)System.out.println("filesys create folder: "+name);
		if (!queryPath(name) || Qaddr>=0)	return false;
		int addr=free_list.allocate();
		Folder folder=new Folder(findINode(addr));
		folder.save();
		Qfather.addEntry(Qname,addr);
		return true;
	}
	
	public boolean removeFolder(String name)
	{
		if (debug)System.out.println("filesys remove folder: "+name);
		if (!queryPath(name) || Qaddr<0 || Qaddr==Folder.STATIC_ADDR)	return false;
		
		INode node=findINode(Qaddr);
		if (node.file_type!=INode.TYPE_FOLDER)	return false;
		
		Folder folder=new Folder(node);
		folder.load();
		if (!folder.isEmpty())	return false;
		
		Qfather.removeEntry(Qname);
		/*if (cur_fold.getLast().inode.getAddr()==Qaddr)
		{
			cur_fold.removeLast();
			cur_path.removeLast();
		}*/
		if (node.link_count==0)
		{
			node.file_type=INode.TYPE_FOLDER_DEL;
			if (node.use_count==0)
			{
				INodes.remove(Qaddr);
				node.free();
			}
		}
		return true;
	}
	
	public boolean changeCurFolder(String name)
	{
		if (debug)System.out.println("filesys change: "+name);
		if (!queryPath(name) || Qaddr<0)	return false;
		
		INode node=findINode(Qaddr);
		if (node.file_type!=INode.TYPE_FOLDER)	return false;
		
		Folder folder=new Folder(node);
		folder.load();
		
		cur_path.clear();
		cur_path.addAll(Qpath);
		cur_path.add(Qname);
		cur_fold.clear();
		cur_fold.addAll(Qfold);
		cur_fold.add(folder);
		
		return true;
	}
	
	public String[] readDir(String name)
	{
		if (debug)System.out.println("filesys read dir: "+name);
		if (!queryPath(name) || Qaddr<0)	return null;
		
		INode node=findINode(Qaddr);
		if (node.file_type!=INode.TYPE_FOLDER)	return null;
		
		Folder folder=new Folder(node);
		folder.load();
		
		return folder.toStrings();
	}
	
	public FileStat getStat(String name)
	{
		if (debug)System.out.println("filesys get stat: "+name);
		if (!queryPath(name) || Qaddr<0)	return null;
		
		INode node=findINode(Qaddr);
		if (node.file_type==INode.TYPE_FILE_DEL)	return null;
		if (node.file_type==INode.TYPE_FOLDER_DEL)	return null;
		
		return new FileStat(Qname,node.file_size,node.getSectors(),node.file_type,Qaddr,node.link_count);
	}
	
	public boolean createLink(String src, String dst)
	{
		if (debug)System.out.println("filesys link: "+dst+"-->"+src);
		if (!queryPath(src) || Qaddr<0)	return false;
		int srcAddr=Qaddr;
		
		if (!queryPath(dst) || Qaddr>=0)	return false;
		
		Qfather.addEntry(Qname,srcAddr);
		return true;
	}
	
	public boolean createSymlink(String src, String dst)
	{
		if (debug)System.out.println("filesys symlink: "+dst+"-->"+src);
		if (!queryPath(dst) || Qaddr>=0)	return false;
		
		int addr=free_list.allocate();
		INode node=findINode(addr);
		node.file_type=INode.TYPE_SYMLINK;
		if (src.charAt(0)!='/')	src=cwdToString()+'/'+src;
		new File(node).write(0,src.getBytes(),0,src.length());

		Qfather.addEntry(Qname,addr);
		return true;
	}
	
	public String cwdToString()
	{
		if (debug)System.out.println("filesys cwd2str");
		String ret="";
		for (Iterator<String> it=cur_path.iterator();it.hasNext();)
		{
			String tmp=it.next();
			if (!tmp.equals(""))	ret+="/"+tmp;
		}
		if (ret.equals(""))	ret="/";
		return ret;
	}
	
	// FilesysGrader
	public int getFreeSize()
	{
		return free_list.length();
	}
	public int getSwapFileSectors()
	{
		queryPath("/Swap");
		INode node=findINode(Qaddr);
		return node.file_size;
	}
}

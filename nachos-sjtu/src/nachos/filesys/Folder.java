package nachos.filesys;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * Folder is a special type of file used to implement hierarchical filesystem.
 * It maintains a map from filename to the address of the file.
 * There's a special folder called root folder with pre-defined address.
 * It's the origin from where you traverse the entire filesystem.
 * 
 * @author starforever
 */
public class Folder extends File
{
	/** the static address for root folder */
	public static int STATIC_ADDR = 1;
	
	private int size;
	
	/** mapping from filename to folder entry */
	private Hashtable<String, FolderEntry> entry;
	
	public Folder(INode inode)
	{
		super(inode);
		size = 4;
		entry = new Hashtable<String, FolderEntry>();
		inode.file_type=INode.TYPE_FOLDER;
	}
	
	/** open a file in the folder and return its address */
	public int open(String filename)
	{
		FolderEntry e=entry.get(filename);
		if (e==null)	return -1;
		return e.addr;
	}
	
	/** create a new file in the folder and return its address */
	public int create(String filename)
	{
		int addr=fileSystem().getFreeList().allocate();
		INode node=fileSystem().findINode(addr);
		addEntry(filename,addr);
		return addr;
	}
	
	/** add an entry with specific filename and address to the folder */
	public void addEntry(String filename, int addr)
	{
		entry.put(filename,new FolderEntry(filename,addr));
		++fileSystem().findINode(addr).link_count;
		save();
	}
	
	/** remove an entry from the folder */
	public void removeEntry(String filename)
	{
		--fileSystem().findINode(entry.remove(filename).addr).link_count;
		save();
	}
	
	public FolderEntry findEntry(String filename)
	{
		load();
		return entry.get(filename);
	}
	
	/** save the content of the folder to the disk */
	public void save()
	{
		int amount=(1+entry.size())<<2;
		for (Iterator<String> it=entry.keySet().iterator();it.hasNext();)
			amount+=4+it.next().length();
		byte[] buf=new byte[amount];
		Arrays.fill(buf,(byte)0);
		
		I2B(entry.size(),buf,0);
		int tot=4; 
		for (Iterator<FolderEntry> it=entry.values().iterator();it.hasNext();)
		{
			FolderEntry e=it.next();
			I2B(e.addr,buf,tot);
			tot+=4;
			I2B(e.name.length(),buf,tot);
			tot+=4;
			System.arraycopy(e.name.getBytes(),0,buf,tot,e.name.length());
			tot+=e.name.length();
		}
		inode.setFileSize(amount);
		if (tot!=amount)
			System.out.println("inode save error");
		write(0,buf,0,amount);
		inode.save();
	}
	
	/** load the content of the folder from the disk */
	public void load()
	{
		int amount=inode.file_size;
		byte[] buf=new byte[amount];
		read(0,buf,0,amount);
		int size=B2I(buf,0),tot=4;
		for (int i=0;i<size;++i)
		{
			int addr=B2I(buf,tot);
			tot+=4;
			int len=B2I(buf,tot);
			tot+=4;
			byte[] str=new byte[len];
			System.arraycopy(buf,tot,str,0,len);
			tot+=len;
			String name=new String(str);
			entry.put(name,new FolderEntry(name,addr));
		}
		if (tot!=amount)
			System.out.println("inode load error");
	}
	public String[] toStrings()
	{
		String[] ret=new String[entry.size()];
		int i=0;
		for (Iterator<String> it=entry.keySet().iterator();it.hasNext();)
			ret[i++]=it.next();
		return ret;
	}
	public boolean isEmpty()
	{
		return entry.isEmpty();
	}
}

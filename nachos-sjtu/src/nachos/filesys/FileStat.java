package nachos.filesys;

public class FileStat
{
	public static final int FILE_NAME_MAX_LEN = 256;
	public static final int NORMAL_FILE_TYPE = 0;
	public static final int DIR_FILE_TYPE = 1;
	public static final int LinkFileType = 2;
	
	public String name;
	public int size;
	public int sectors;
	public int type;
	public int inode;
	public int links;
	public FileStat(String n,int siz,int sec,int typ,int node,int lnks)
	{
		name = n;
		size = siz;
		sectors = sec;
		if (typ==INode.TYPE_FILE)	type=NORMAL_FILE_TYPE;
		else
		if (typ==INode.TYPE_FOLDER)	type=DIR_FILE_TYPE;
		else
		if (typ==INode.TYPE_SYMLINK)	type=LinkFileType;
		else	type = -1;
		inode = node;
		links = lnks;
	}
}

package nachos.filesys;

import nachos.machine.Disk;
import nachos.machine.Machine;
import nachos.threads.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * INode contains detail information about a file.
 * Most important among these is the list of sector numbers the file occupied, 
 * it's necessary to find all the pieces of the file in the filesystem.
 * 
 * @author starforever
 */
public class INode
{
	private static int sectorSize=Disk.SectorSize;
	
	/** represent a system file(free list) */
	public static int TYPE_SYSTEM = 0;
	
	/** represent a folder */
	public static int TYPE_FOLDER = 1;
	
	/** represent a normal file */
	public static int TYPE_FILE = 2;
	
	/** represent a normal file that is marked as delete */
	public static int TYPE_FILE_DEL = 3;
	
	/** represent a symbolic link file */
	public static int TYPE_SYMLINK = 4;
	
	/** represent a folder that are not valid */
	public static int TYPE_FOLDER_DEL = 5;
	
	/** the reserve size(in byte) in the first sector */
	private static final int FIRST_SEC_RESERVE = 16;
	
	/** size of the file in bytes */
	int file_size;
	
	/** the type of the file */
	int file_type;
	
	/** the number of programs that have access on the file */
	int use_count;
	
	/** the number of links on the file */
	int link_count;
	
	/** maintain all the sector numbers this file used in order */
	private LinkedList<Integer> sec_addr;
	
	/** the first address */
	private int addr;
	
	/** the extended address */
	private LinkedList<Integer> addr_ext;
	
	private Lock lock;
	
	public void acquire()
	{
		lock.acquire();
	}
	public void release()
	{
		lock.release();
	}
	public INode(int addr)
	{
		file_size = 0;
		file_type = TYPE_FILE;
		use_count = 0;
		link_count = 0;
		sec_addr = new LinkedList<Integer>();
		this.addr = addr;
		addr_ext = new LinkedList<Integer>();
		lock=new Lock();
	}
	
	private FreeList freeList()
	{
		return FilesysKernel.realFileSystem.getFreeList();
	}
	/** get the sector number of a position in the file	*/
	public int getSector(int pos)
	{
		return sec_addr.get(pos);
	}
	
	public int getSectors()
	{
		return sec_addr.size();
	}
	
	/** change the file size and adjust the content in the inode accordingly */
	public void setFileSize(int size)
	{
		int cursec=(size-1)/sectorSize+1;
		if (size==0)	cursec=0;
		int oldsec=(file_size-1)/sectorSize+1;
		if (file_size==0)	oldsec=0;
		file_size=size;
		if (cursec==oldsec)	return;
		
		for (int i=oldsec;i<cursec;++i)
			sec_addr.add(freeList().allocate());
		for (int i=cursec;i<oldsec;++i)
			freeList().deallocate(sec_addr.removeLast());
		
//		save();
		// do either
	}
	
	/** free the disk space occupied by the file(including inode) */
	public void free()
	{
		for (Iterator<Integer> it=sec_addr.iterator();it.hasNext();)
			freeList().deallocate(it.next());

		for (Iterator<Integer> it=addr_ext.iterator();it.hasNext();)
			freeList().deallocate(it.next());
		
		freeList().deallocate(addr);
		sec_addr.clear();
		addr_ext.clear();
	}

	/** load inode content from the disk */
	public void load()
	{
		if (RealFileSystem.debug)System.out.println("inode addr "+addr+" loading, size = "+file_size);
		byte[] buf=new byte[sectorSize];
		Machine.synchDisk().readSector(addr,buf,0);
		
		file_size=File.B2I(buf,0);
		file_type=File.B2I(buf,4);
		use_count=File.B2I(buf,8);
		link_count=File.B2I(buf,12);
		int addr_ext_size=File.B2I(buf,16);
		int sec_addr_size=File.B2I(buf,20);
		
		int tot=24,secIndex=-1;
		for (int i=0;i<addr_ext_size;++i)
		{
			if (tot%sectorSize==0)
				Machine.synchDisk().readSector(addr_ext.get(++secIndex),buf,0);
			int tmp=File.B2I(buf,tot%sectorSize);
			addr_ext.add(tmp);
			tot+=4;
		}
		for (int i=0;i<sec_addr_size;++i)
		{
			if (tot%sectorSize==0)
				Machine.synchDisk().readSector(addr_ext.get(++secIndex),buf,0);
			int tmp=File.B2I(buf,tot%sectorSize);
			sec_addr.add(tmp);
			tot+=4;
		}
		if (addr_ext.size()!=addr_ext_size || sec_addr.size()!=sec_addr_size)
			System.out.println("inode load error");
	}
	
	/** save inode content to the disk */
	public void save()
	{
		if (RealFileSystem.debug)System.out.println("inode addr "+addr+" saving, size = "+file_size);
		// calc #bytes and allocate fair amount of sectors
		int amount=(4+2+sec_addr.size()+addr_ext.size())<<2;
		while (amount>(1+addr_ext.size())*sectorSize)
		{
			addr_ext.add(freeList().allocate());
			amount+=4;
		}
		while (!addr_ext.isEmpty() && amount<addr_ext.size()*sectorSize)
		{
			freeList().deallocate(addr_ext.removeLast());
			amount-=4;
		}
		
		byte[] buf=new byte[sectorSize];
		Arrays.fill(buf,(byte)0);
		int tot=24,secNum=addr,secIndex=-1;
		
		File.I2B(file_size,buf,0);
		File.I2B(file_type,buf,4);
		File.I2B(use_count,buf,8);
		File.I2B(link_count,buf,12);
		File.I2B(addr_ext.size(),buf,16);
		File.I2B(sec_addr.size(),buf,20);
		
		// save addr_ext first in order to avoid more latency when load
		for (int i=0;i<addr_ext.size();++i)
		{
			if (tot%sectorSize==0)
			{
				Machine.synchDisk().writeSector(secNum,buf,0);
				Arrays.fill(buf,(byte)0);
				secNum=addr_ext.get(++secIndex);
			}
			File.I2B(addr_ext.get(i),buf,tot%sectorSize);
			tot+=4;
		}
		
		for (int i=0;i<sec_addr.size();++i)
		{
			if (tot%sectorSize==0)
			{
				Machine.synchDisk().writeSector(secNum,buf,0);
				secNum=addr_ext.get(++secIndex);
				Arrays.fill(buf,(byte)0);
			}
			File.I2B(sec_addr.get(i),buf,tot%sectorSize);
			tot+=4;
		}
		Machine.synchDisk().writeSector(secNum,buf,0);
		if (tot!=amount)
			System.out.println("inode save error");
	}
	public int getAddr()
	{
		return addr;
	}
}

package nachos.threads;

import nachos.machine.*;

/**
 * An implementation of condition variables that disables interrupt()s for
 * synchronization.
 * 
 * <p>
 * You must implement this.
 * 
 * @see nachos.threads.Condition
 */
public class Condition2 { // task 2
	
	/**
	 * Allocate a new condition variable.
	 * 
	 * @param conditionLock
	 *            the lock associated with this condition variable. The current
	 *            thread must hold this lock whenever it uses <tt>sleep()</tt>,
	 *            <tt>wake()</tt>, or <tt>wakeAll()</tt>.
	 */
	ThreadQueue Hangers=ThreadedKernel.scheduler.newThreadQueue(false);
	
	public Condition2(Lock conditionLock) {
		this.conditionLock = conditionLock;
	}

	/**
	 * Atomically release the associated lock and go to sleep on this condition
	 * variable until another thread wakes it using <tt>wake()</tt>. The current
	 * thread must hold the associated lock. The thread will automatically
	 * reacquire the lock before <tt>sleep()</tt> returns.
	 */
	public void sleep() {
		boolean intStatus=Machine.interrupt().disable();
		
		Lib.assertTrue(conditionLock.isHeldByCurrentThread());

		Hangers.waitForAccess(KThread.currentThread());
		conditionLock.release();
		KThread.sleep();
		conditionLock.acquire();
		
		Machine.interrupt().restore(intStatus);
	}

	/**
	 * Wake up at most one thread sleeping on this condition variable. The
	 * current thread must hold the associated lock.
	 */
	public void wake() {
		boolean intStatus=Machine.interrupt().disable();
		
		Lib.assertTrue(conditionLock.isHeldByCurrentThread());
		
		KThread T=Hangers.nextThread();
		if (T!=null)
			T.ready();
		
		Machine.interrupt().restore(intStatus);
	}

	/**
	 * Wake up all threads sleeping on this condition variable. The current
	 * thread must hold the associated lock.
	 */
	public void wakeAll() {
		boolean intStatus=Machine.interrupt().disable();
		
		Lib.assertTrue(conditionLock.isHeldByCurrentThread());
		
		for (KThread T=Hangers.nextThread();T!=null;T=Hangers.nextThread())
			T.ready();
		
		Machine.interrupt().restore(intStatus);
	}

	private Lock conditionLock;
}

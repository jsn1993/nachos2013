package nachos.threads;

import java.util.Iterator;
import nachos.machine.*;
import nachos.threads.PriorityScheduler.PriorityQueue;
import nachos.threads.PriorityScheduler.ThreadState;

/**
 * A scheduler that chooses threads using a lottery.
 * 
 * <p>
 * A lottery scheduler associates a number of tickets with each thread. When a
 * thread needs to be dequeued, a random lottery is held, among all the tickets
 * of all the threads waiting to be dequeued. The thread that holds the winning
 * ticket is chosen.
 * 
 * <p>
 * Note that a lottery scheduler must be able to handle a lot of tickets
 * (sometimes billions), so it is not acceptable to maintain state for every
 * ticket.
 * 
 * <p>
 * A lottery scheduler must partially solve the priority inversion problem; in
 * particular, tickets must be transferred through locks, and through joins.
 * Unlike a priority scheduler, these tickets add (as opposed to just taking the
 * maximum).
 */
public class LotteryScheduler extends PriorityScheduler {
	/**
	 * Allocate a new lottery scheduler.
	 */
	public LotteryScheduler() {
	}

	/**
	 * Allocate a new lottery thread queue.
	 * 
	 * @param transferPriority
	 *            <tt>true</tt> if this queue should transfer tickets from
	 *            waiting threads to the owning thread.
	 * @return a new lottery thread queue.
	 */
	public ThreadQueue newThreadQueue(boolean transferPriority) {
		return new LotteryQueue(transferPriority);
	}
	
	public void setPriority(KThread thread, int priority) {
		Lib.assertTrue(Machine.interrupt().disabled());

		Lib.assertTrue(priority >= priorityMinimum
				&& priority <= Integer.MAX_VALUE);

		getThreadState(thread).setPriority(priority);
	}

	public boolean increasePriority()
	{
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		int priority = getPriority(thread);
		if (priority == Integer.MAX_VALUE)
		{
		  Machine.interrupt().restore(intStatus); // bug identified by Xiao Jia @ 2011-11-04
			return false;
		}

		setPriority(thread, priority + 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}

	public boolean decreasePriority()
	{
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		int priority = getPriority(thread);
		if (priority == Integer.MAX_VALUE)
		{
		  Machine.interrupt().restore(intStatus); // bug identified by Xiao Jia @ 2011-11-04
			return false;
		}

		setPriority(thread, priority - 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}
	
	protected class LotteryQueue extends PriorityQueue
	{
		public LotteryQueue(boolean transferPriority)
		{
			super(transferPriority);
		}
		protected ThreadState pickNextThread()
		{
			int lotteryCount=0;
			if (elems.isEmpty())	return null;
			int[] P=new int[elems.size()]; 
			for (int i=0;i<elems.size();++i)
				lotteryCount+=(P[i]=elems.get(i).getEffectivePriority());
			if (lotteryCount==0)	return null;
			int n=Lib.random(lotteryCount);
			for (int i=0;i<elems.size();++i)
			if (P[i]>=n)	return elems.get(i); 
			else	n-=P[i];// 2+3+5+4+2 7-th: 7-2-3=2  <5, 3rd thread
			return null;
		}
	}
	protected class LotteryState extends ThreadState
	{
		public LotteryState(KThread thread)
		{
			super(thread);
		} 
		public int getEffectivePriority()//getTickets
		{
			int tickets=getPriority();
			// through join
			for (Iterator<ThreadState> it2=((PriorityQueue)thread.Joiners).elems.iterator();it2.hasNext();)
				tickets+=it2.next().getEffectivePriority();
			// through resources
			for (Iterator<PriorityQueue> it=Followers.iterator();it.hasNext();)
			{
				PriorityQueue q=it.next();
				if (!q.transferPriority)	continue;
				for (Iterator<ThreadState> it2=q.elems.iterator();it2.hasNext();)
					tickets+=it2.next().getEffectivePriority();
			}
			return tickets;
		}
	}
}

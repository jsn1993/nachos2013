package nachos.threads;

import java.util.*;
/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	LinkedList<Condition2> pendingSpeakers=new LinkedList<Condition2>();
	LinkedList<Condition2> pendingListeners=new LinkedList<Condition2>();
	LinkedList<Integer> whatToSpeak=new LinkedList<Integer>();
	Lock lock=new Lock();
	public Communicator() {
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) { // task 4
		lock.acquire();
		assert(lock.isHeldByCurrentThread());

		Condition2 newSpeaker=new Condition2(lock);
		//do not need to wait because N is not fixed, i.e. produce as many as possible
		//similar to Philosophy P149 monitor
		pendingSpeakers.add(newSpeaker);
		whatToSpeak.add(word);
		
		if (!pendingListeners.isEmpty())	pendingListeners.removeFirst().wake();
		newSpeaker.sleep(); // need to sleep?
		
		lock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */
	public int listen() {
		lock.acquire();
		assert(lock.isHeldByCurrentThread());

		Condition2 newListener=new Condition2(lock);
		// wait(empty)
		while (pendingSpeakers.isEmpty())
		{
			pendingListeners.add(newListener);
			newListener.sleep();
		}
		pendingSpeakers.removeFirst().wake();
		lock.release();
		return whatToSpeak.removeFirst();
	}
}

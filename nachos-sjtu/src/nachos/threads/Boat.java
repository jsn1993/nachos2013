package nachos.threads;

import java.util.*;
import nachos.ag.BoatGrader;

public class Boat {
	static BoatGrader bg;
	static Lock lock;
	static Condition ctrl;
	static int pendingAdults,pendingChildren;
	static boolean boatArrived,pilotReady; // 1 -- Oahu    0 -- Molokai

	public static void selfTest() {
		BoatGrader b = new BoatGrader();

		System.out.println("\n ***Testing Boats with only 2 children***");
		begin(0, 2, b);

		// System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
		// begin(1, 2, b);

		// System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
		// begin(3, 3, b);
	}

	public static void begin(int adults, int children, BoatGrader b) {
		// Store the externally generated autograder in a class
		// variable to be accessible by children.
		bg = b;

		// Instantiate global variables here
		lock=new Lock();
		ctrl=new Condition(lock);
		pendingAdults=adults;
		pendingChildren=children;
		boatArrived=false;
		// Create threads here. See section 3.4 of the Nachos for Java
		// Walkthrough linked from the projects page.
		
		LinkedList<KThread> list=new LinkedList<KThread>();
		for (int i=0;i<pendingAdults;++i)
		{
			Runnable r = new Runnable() {
				public void run() {
					AdultItinerary();
				}
			};
			KThread t = new KThread(r);
			t.setName("Adult Thread "+i);
			list.add(t);
			t.fork();
		}
		for (int i=0;i<pendingChildren;++i)
		{
			Runnable r = new Runnable() {
				public void run() {
					ChildItinerary();
				}
			};
			KThread t = new KThread(r);
			t.setName("Child Thread "+i);
			list.add(t);
			t.fork();
		}
		for (Iterator<KThread> it=list.iterator();it.hasNext();)
			it.next().join();
	}

	static void AdultItinerary() {
		/*
		 * This is where you should put your solutions. Make calls to the
		 * BoatGrader to show that it is synchronized. For example:
		 * bg.AdultRowToMolokai(); indicates that an adult has rowed the boat
		 * across to Molokai
		 */
		lock.acquire();
		
		while (boatArrived==true  // at Oahu
			|| pendingChildren>=2) // ATTENTION!!! ENSURING AT LEAST ONE CHILD IS AT Molokai IN ORDER TO RIDE BACK!!! 1.5 hours wasted :(   
			ctrl.sleep();
		boatArrived=true;
		--pendingAdults; // 1 adult
		//System.out.println("pendingAdults: "+pendingAdults);
		bg.AdultRowToMolokai();
		ctrl.wakeAll();
		
		lock.release();
	}

	static void ChildItinerary() {
		lock.acquire();

		boolean arrived=false;
		
		while (pendingChildren>0 || pendingAdults>0)
		if (arrived) // arrived, a.k.a. at Molokai
			if (boatArrived==false) // boat at Oahu and I'm at Molokai, do nothing
				ctrl.sleep();
			else // both boat and I are at Molokai, ride back
			{
				arrived=false;
				boatArrived=false;
				++pendingChildren;
				//System.out.println("pendingChildren: "+pendingChildren);
				bg.ChildRowToOahu();
				ctrl.wakeAll();
			}
		else // not arrived, a.k.a. at Oahu
			if (boatArrived==false) // boat at Oahu and I'm at Oahu
			{
				if (pendingChildren>=2) // 2 children 
				{
					if (pilotReady==true)
					{
						arrived=true;
						boatArrived=true;
						pilotReady=false;
						pendingChildren-=2;
						//System.out.println("pendingChildren: "+pendingChildren);
						bg.ChildRowToMolokai();
						bg.ChildRideToMolokai();
						ctrl.wakeAll();
					}
					else
					{
						pilotReady=true;
						arrived=true;
					}
				}
				else // pending children==1 and it's me!
					if (pendingAdults==0) // 1 children
					{
						arrived=true;
						boatArrived=true;
						--pendingChildren;
						//System.out.println("pendingChildren: "+pendingChildren);
						bg.ChildRowToMolokai();
						ctrl.wakeAll();
					}
					else
						ctrl.sleep();
			}
			else // boat at Molokai and I'm at Oahu, do nothing
				ctrl.sleep();
		lock.release();
	}
}
package nachos.userprog;

import nachos.filesys.RealFileSystem;
import nachos.machine.*;
import nachos.threads.*;
import java.util.*;

import java.io.EOFException;

/**
 * Encapsulates the state of a user process that is not contained in its user
 * thread (or threads). This includes its address translation state, a file
 * table, and information about the program being executed.
 * 
 * <p>
 * This class is extended by other classes to support additional functionality
 * (such as additional syscalls).
 * 
 * @see nachos.vm.VMProcess
 * @see nachos.network.NetProcess
 */
public class UserProcess {
	
	protected static Lock lock=new Lock(); //synchronization for processcount
	protected static HashMap<Integer,UserProcess> UserProcesses=new HashMap<Integer,UserProcess>();
	protected static int processCount=0;
	protected static int inExecute=0;
	protected int numPhysPages,processID,fileCount,exitCode;
	boolean normalExit;
	protected HashMap<Integer,OpenFile> OpenFiles;
	
	protected UThread myThread=null;
	protected HashSet<Integer> myChildren;
	
	/**
	 * Allocate a new process.
	 */
	protected void resetPageTableByVPN(int vpn)
	{
		pageTable[vpn]=new TranslationEntry(vpn,0,false,false,false,false);
	}
	protected boolean illegalTableEntry(int vpn)
	{
		if (vpn<0 || vpn>=numPhysPages)	return true;
		TranslationEntry e=findPage(vpn);
		if (pageTable==null || e==null)	return true;
		return false;
	}
	
	public UserProcess() {
		numPhysPages = Machine.processor().getNumPhysPages();
		pageTable = new TranslationEntry[numPhysPages];
		for (int i = 0; i < numPhysPages; i++)
			resetPageTableByVPN(i);
		
		exitCode=0;
		fileCount=2;
		myChildren=new HashSet<Integer>();
		OpenFiles=new HashMap<Integer,OpenFile>();
		OpenFiles.put(0,UserKernel.console.openForReading());
		OpenFiles.put(1,UserKernel.console.openForWriting());
		lock.acquire();
		UserProcesses.put(processID=processCount++,this);
		lock.release();
		
	}

	/**
	 * Allocate and return a new process of the correct class. The class name is
	 * specified by the <tt>nachos.conf</tt> key
	 * <tt>Kernel.processClassName</tt>.
	 * 
	 * @return a new process of the correct class.
	 */
	public static UserProcess newUserProcess() {
		return (UserProcess) Lib.constructObject(Machine.getProcessClassName());
	}
	public int PID()
	{
		return processID;
	}
	/**
	 * Execute the specified program with the specified arguments. Attempts to
	 * load the program, and then forks a thread to run it.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the program was successfully executed.
	 */
	
	public boolean execute(String name, String[] args) {
		if (!load(name, args))
			return false;
		
		lock.acquire();
		++inExecute;
		lock.release();
		
		myThread=new UThread(this);
		myThread.setName(name);
		myThread.fork();
		
		return true;
	}
	int fOpen(String fname,boolean truncate)
	{
		if (fname==null)	return -1;
		
		OpenFile file=ThreadedKernel.fileSystem.open(fname,truncate);
		if (file==null)	return -1;
		
		int id;
		OpenFiles.put(id=fileCount++,file);
		return id;
	}
	public boolean applyPages(boolean readonly,int vpn,int amount)
	{
		LinkedList<TranslationEntry> ready=new LinkedList<TranslationEntry>();
		for (int i=vpn;i<amount+vpn;++i)
		{
			int ppn=UserKernel.takePage();
			if (ppn<0 || i>=numPhysPages)
			{
				for (Iterator<TranslationEntry> it=ready.iterator();it.hasNext();)
				{
					TranslationEntry e=it.next();
					--numPages;
					UserKernel.returnPage(e.ppn);
					resetPageTableByVPN(e.vpn);
				}
				return false;
			}
			++numPages;
			pageTable[i]=new TranslationEntry(i,ppn,true,readonly,false,false);
			ready.add(pageTable[i]);
		}
		return true;
	}
	public boolean cleanUp()
	{
		coff.close();
		for (Iterator<Map.Entry<Integer,OpenFile>> it=OpenFiles.entrySet().iterator();it.hasNext();)
		{
			Map.Entry<Integer,OpenFile> file=it.next();
			file.getValue().close();
		}
		for (int i=0;i<pageTable.length;++i)
		if (pageTable[i].valid)
		{
			UserKernel.returnPage(pageTable[i].ppn);
			resetPageTableByVPN(i);
		}
		return false;
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		Machine.processor().setPageTable(pageTable);
	}

	public int offset(int vaddr)
	{
		return vaddr%pageSize;
	}
	public int vpn(int vaddr)
	{
		return vaddr>>UserKernel.offset;
	}
	public int vaddr(int vpn,int off)
	{
		return (vpn<<UserKernel.offset)+off;
	}
	public int paddr(int ppn,int off)
	{
		return (ppn<<UserKernel.offset)+off;
	}
	/**
	 * Read a null-terminated string from this process's virtual memory. Read at
	 * most <tt>maxLength + 1</tt> bytes from the specified address, search for
	 * the null terminator, and convert it to a <tt>java.lang.String</tt>,
	 * without including the null terminator. If no null terminator is found,
	 * returns <tt>null</tt>.
	 * 
	 * @param vaddr
	 *            the starting virtual address of the null-terminated string.
	 * @param maxLength
	 *            the maximum number of characters in the string, not including
	 *            the null terminator.
	 * @return the string read, or <tt>null</tt> if no null terminator was
	 *         found.
	 */
	public String readVirtualMemoryString(int vaddr, int maxLength) {
		Lib.assertTrue(maxLength >= 0);

		byte[] bytes = new byte[maxLength + 1];

		int bytesRead = readVirtualMemory(vaddr, bytes);

		for (int length = 0; length < bytesRead; length++) {
			if (bytes[length] == 0)
				return new String(bytes, 0, length);
		}

		return null;
	}

	/**
	 * Transfer data from this process's virtual memory to all of the specified
	 * array. Same as <tt>readVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data) {
		return readVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from this process's virtual memory to the specified array.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the
	 *            array.
	 * @return the number of bytes successfully transferred.
	 */
	public TranslationEntry findPage(int vpn)
	{
		return pageTable[vpn];
	}
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();
		
		if (vaddr < 0 || vaddr >= memory.length)
			return 0;
		int amount=0,l=length,o=offset;
		do
		{
			TranslationEntry e=findPage(vpn(vaddr));
			if (e==null || !e.valid)	return 0;
			e.used=true;
			int inThisPage=Math.min(l, pageSize - offset(vaddr));
			System.arraycopy(memory, paddr(e.ppn,offset(vaddr)), data, o, inThisPage);
			l-=inThisPage;
			amount+=inThisPage;
			o+=inThisPage;
			vaddr=vaddr(vpn(vaddr)+1,0);
		}
		while (l>0);

		return amount;
	}

	/**
	 * Transfer all data from the specified array to this process's virtual
	 * memory. Same as <tt>writeVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data) {
		return writeVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from the specified array to this process's virtual memory.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual
	 *            memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();

		if (vaddr < 0 || vaddr >= memory.length)
			return 0;
		
		int amount=0,l=length,o=offset;
		do
		{
			TranslationEntry e=findPage(vpn(vaddr));
			if (e==null || !e.valid)	return 0;
			e.dirty=true;
			int inThisPage=Math.min(l, pageSize - offset(vaddr));
			System.arraycopy(data, o, memory, paddr(e.ppn,offset(vaddr)), inThisPage);
			l-=inThisPage;
			amount+=inThisPage;
			o+=inThisPage;
			vaddr=vaddr(vpn(vaddr)+1,0);
		}
		while (l>0);
		
		return amount;
	}

	/**
	 * Load the executable with the specified name into this process, and
	 * prepare to pass it the specified arguments. Opens the executable, reads
	 * its header information, and copies sections and arguments into this
	 * process's virtual memory.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the executable was successfully loaded.
	 */
	protected boolean load(String name, String[] args) {
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");

		OpenFile executable = ThreadedKernel.fileSystem.open(name, false);
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			return false;
		}
		try {
			coff = new Coff(executable);
		} catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			return false;
		}

		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				return false;
			}
			if (!applyPages(section.isReadOnly(),numPages,section.getLength()))
				return cleanUp();
		}

		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 0;
		for (int i = 0; i < args.length; i++) {
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			coff.close();
			Lib.debug(dbgProcess, "\targuments too long");
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();

		// next comes the stack; stack pointer initially points to top of it
		if (!applyPages(false,numPages,stackPages))
			return cleanUp();
		
		initialSP = numPages * pageSize;

		// and finally reserve 1 page for arguments
		if (!applyPages(false,numPages,1))
			return cleanUp();

		if (!loadSections())
			return false;
		// store arguments in last page
		int entryOffset = (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;

		this.argc = args.length;
		this.argv = entryOffset;

		for (int i = 0; i < argv.length; i++) {
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
			Lib
					.assertTrue(writeVirtualMemory(entryOffset,
							stringOffsetBytes) == 4);
			entryOffset += 4;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset, argv[i]) == argv[i].length);
			stringOffset += argv[i].length;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset,
							new byte[] { 0 }) == 1);
			stringOffset += 1;
		}
		return true;
	}

	/**
	 * Allocates memory for this process, and loads the COFF sections into
	 * memory. If this returns successfully, the process will definitely be run
	 * (this is the last step in process initialization that can fail).
	 * 
	 * @return <tt>true</tt> if the sections were successfully loaded.
	 */
	protected boolean loadSections() {
		if (numPages > Machine.processor().getNumPhysPages()) {
			coff.close();
			Lib.debug(dbgProcess, "\tinsufficient physical memory");
			return false;
		}

		// load sections
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");

			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;
				
				if (illegalTableEntry(vpn))	return false;
				TranslationEntry e=findPage(vpn);
				section.loadPage(i,e.ppn);
			}
		}

		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
	}

	/**
	 * Initialize the processor's registers in preparation for running the
	 * program loaded into this process. Set the PC register to point at the
	 * start function, set the stack pointer register to point at the top of the
	 * stack, set the A0 and A1 registers to argc and argv, respectively, and
	 * initialize all other registers to 0.
	 */
	public void initRegisters() {
		Processor processor = Machine.processor();

		// by default, everything's 0
		for (int i = 0; i < Processor.numUserRegisters; i++)
			processor.writeRegister(i, 0);

		// initialize PC and SP according
		processor.writeRegister(Processor.regPC, initialPC);
		processor.writeRegister(Processor.regSP, initialSP);

		// initialize the first two argument registers to argc and argv
		processor.writeRegister(Processor.regA0, argc);
		processor.writeRegister(Processor.regA1, argv);
	}
	void exit(int abnormal)
	{
		if (abnormal==0)	normalExit=true;
		else	normalExit=false;
		cleanUp();
		boolean allExit=false;
		lock.acquire();
		if (--inExecute==0)	allExit=true;
		lock.release();
		
		if (allExit)
			Kernel.kernel.terminate();
		KThread.finish();
	}
	/**
	 * Handle the halt() system call.
	 */
	protected int handleHalt() {

		if (PID()!=0)	return -1; //Any other process should ignore the syscall and return immediately	
		
		exit(0); //The first process, executed by UserKernel.run()) should be allowed to execute this syscall.
		
		Lib.assertNotReached("Machine.halt() did not halt machine!");
		return 0;
	}
	
	protected int handleExit(int a0) //void exit(int status)
	{
		exitCode=a0;
		exit(0);
		return a0;
	}
	protected int handleExec(int a0,int a1,int a2) //int exec(char *name, int argc, char **argv)
	{
		String fname=readVirtualMemoryString(a0,256);
		if (fname==null || a1<0)	return -1;
		
		String argv[]=new String[a1];
		for (int i=0;i<a1;++i)
		{
			byte[] buf=new byte[4];
			if (readVirtualMemory(a2+(i<<2),buf)!=4)	return -1;
			
			argv[i]=readVirtualMemoryString((int) ((((int) buf[0] & 0xFF))|(((int) buf[1] & 0xFF) << 8)
											|(((int) buf[2] & 0xFF) << 16)|(((int) buf[3] & 0xFF) << 24)),256);
			if (argv[i]==null)	return -1;
		}
		
		UserProcess Child=UserProcess.newUserProcess();
		if (!Child.execute(fname,argv))	return -1;
		myChildren.add(Child.PID());
		
		return Child.PID();
	}
	protected int handleJoin(int a0,int a1) //int join(int pid, int *status)
	{
		if (!myChildren.contains(a0))	return -1;// only parent can join to it
		
		UserProcess child=UserProcesses.get(a0);
		
		child.myThread.join();
		byte[] tmp=new byte[4];
		tmp[0]=(byte)(child.exitCode & 0xFF);
		tmp[1]=(byte)((child.exitCode >> 8) & 0xFF);
		tmp[2]=(byte)((child.exitCode >> 16) & 0xFF);
		tmp[3]=(byte)((child.exitCode >> 24) & 0xFF);
		if (writeVirtualMemory(a1,tmp)!=4)	return -1;
		
		if (child.normalExit)	return 1;
		return 0;
	}
	protected int handleCreat(int a0) //int creat(char *name)
	{
		return fOpen(readVirtualMemoryString(a0,256),true);
	}
	protected int handleOpen(int a0) //int open(char *name)
	{
		return fOpen(readVirtualMemoryString(a0,256),false);
	}
	protected int handleRead(int a0,int a1,int a2) //int read(int fd, char *buffer, int size)
	{
		OpenFile file=OpenFiles.get(a0);
		if (file==null)	return -1;
		
		byte[] buf=new byte[a2];
		
		int readLen=file.read(buf,0,a2);
		if (readLen<0)	return -1;
		
		int writeLen=writeVirtualMemory(a1,buf,0,readLen);
		if (writeLen!=readLen)	return -1; // no enough space
		
		return writeLen;
	}
	protected int handleWrite(int a0,int a1,int a2) //int write(int fd, char *buffer, int size)
	{
		OpenFile file=OpenFiles.get(a0);
		if (file==null)	return -1;
		
		byte[] buf=new byte[a2];
		
		int readLen=readVirtualMemory(a1,buf,0,a2);
		if (readLen!=a2)	return -1;
		return file.write(buf,0,a2);
	}
	protected int handleClose(int a0) //int close(int fd)
	{
		OpenFile file=OpenFiles.get(a0);
		if (file==null)	return -1;
		
		file.close();
		OpenFiles.remove(a0);
		return 0;
	}
	protected int handleUnlink(int a0) //int unlink(char *name)
	{
		String fname=readVirtualMemoryString(a0,256);
		if (fname==null || ThreadedKernel.fileSystem.remove(fname)==false)	return -1;
		return 0;
	}

	protected static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2,
			syscallJoin = 3, syscallCreate = 4, syscallOpen = 5,
			syscallRead = 6, syscallWrite = 7, syscallClose = 8,
			syscallUnlink = 9;
	protected static final String[] syscallName={"Halt","Exit","Exec","Join","Create","Open","Read","Write","Close","Unlink"};
	
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
        switch (syscall) {
        case syscallHalt:
            return handleHalt(); //ok

        case syscallExit:
            return handleExit(a0);
            
        case syscallExec:
            return handleExec(a0, a1, a2);

        case syscallJoin:
            return handleJoin(a0, a1);
            
        case syscallCreate:
            return handleCreat(a0);//ok

        case syscallOpen:
            return handleOpen(a0);//ok

        case syscallRead:
            return handleRead(a0, a1, a2);//ok

        case syscallWrite:
            return handleWrite(a0, a1, a2);//ok

        case syscallClose:
            return handleClose(a0);//ok

        case syscallUnlink:
            return handleUnlink(a0);//ok

        default:
            Lib.debug(dbgProcess, "Unknown syscall " + syscall);
            exit(1);
            Lib.assertNotReached("Unknown system call!");
        }
        return 0;
    }
	
	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionSyscall:
			int result = handleSyscall(processor.readRegister(Processor.regV0),
					processor.readRegister(Processor.regA0), processor
							.readRegister(Processor.regA1), processor
							.readRegister(Processor.regA2), processor
							.readRegister(Processor.regA3));
			processor.writeRegister(Processor.regV0, result);
			processor.advancePC();
			break;

		default:
			Lib.debug(dbgProcess, "Unexpected exception: "
					+ Processor.exceptionNames[cause]);
			exit(cause);
			Lib.assertNotReached("Unexpected exception");
		}
	}

	/** The program being run by this process. */
	protected Coff coff;

	/** This process's page table. */
	protected TranslationEntry[] pageTable;
	/** The number of contiguous pages occupied by the program. */
	protected int numPages;

	/** The number of pages in the program's stack. */
	protected final int stackPages = Config.getInteger("Processor.numStackPages", 8);

	protected int initialPC, initialSP;
	protected int argc, argv;

	protected static final int pageSize = Processor.pageSize;
	protected static final char dbgProcess = 'a';
}

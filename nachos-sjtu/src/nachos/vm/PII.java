package nachos.vm;

public class PII
{
	int first,second;
	public PII(int f,int s)
	{
		first=f;second=s;
	}
	public int hashCode()
	{
		return (first*31)^second;
	}
	public boolean equals(Object obj)
	{
		if (obj instanceof PII)
		{
			PII tmp=(PII)obj;
			return (first==tmp.first) && (second==tmp.second);
		}
		return false;
	}
}
package nachos.vm;

import nachos.machine.Coff;
import nachos.machine.CoffSection;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.userprog.UserProcess;
import nachos.threads.*;

import java.io.EOFException;
import java.util.*;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	HashMap<Integer,PII> Lazy;
	protected static Lock memLock=new Lock();
	TranslationEntry[] tlbBackup;
	int tlbSize;
	public VMProcess() {
		super();
		RoundRobin=0;
		tlbBackup=new TranslationEntry[tlbSize=Machine.processor().getTLBSize()];
		Lazy=new HashMap<Integer,PII>();
	}
	
	public static VMProcess newUserProcess() {
		return (VMProcess) Lib.constructObject(Machine.getProcessClassName());
	}
	
//	Override UserProcess
	@Override
	public void resetPageTableByVPN(int vpn){}
	@Override
	public boolean cleanUp()
	{
		coff.close();
		for (Iterator<Map.Entry<Integer,OpenFile>> it=OpenFiles.entrySet().iterator();it.hasNext();)
		{
			Map.Entry<Integer,OpenFile> file=it.next();
			file.getValue().close();
		}
		numPages-=VMKernel.resetEntry(processID);
		return false;
	}
	@Override
	public boolean applyPages(boolean readonly,int vpn,int amount)
	{
		for (int i=vpn;i<amount+vpn;++i)
		{
			//int ppn=VMKernel.takePage(processID,i,readonly,Lazy);
			++numPages;
		}
		return true;
	}
	@Override
	public TranslationEntry findPage(int vpn)
	{
		return VMKernel.findPage(processID,vpn);
	}
	@Override
	public void saveState() {
		boolean status = Machine.interrupt().disable();
		for (int i=0;i<tlbBackup.length;++i)
		{
			TranslationEntry tlb=Machine.processor().readTLBEntry(i);
			if (tlb!=null && tlb.valid)
			{
				TranslationEntry e=findPage(tlb.vpn);
				if (e==null)	continue;
				e.dirty|=tlb.dirty;
				e.used|=tlb.used;
			}
			tlbBackup[i]=tlb;
			Machine.processor().writeTLBEntry(i, new TranslationEntry());
		}
		
		Machine.interrupt().restore(status);
	}
	@Override
	public void restoreState() {
		boolean status = Machine.interrupt().disable();

		for (int i=0;i<tlbBackup.length;++i)
		if (tlbBackup[i]==null)
		{
			tlbBackup[i]=new TranslationEntry();
			Machine.processor().writeTLBEntry(i,new TranslationEntry(tlbBackup[i]));
		}
		else
		{
			int vpn=tlbBackup[i].vpn;
			int ppn=tlbBackup[i].ppn;
			TranslationEntry e=findPage(vpn);
			if (e!=null && e.valid)	Machine.processor().writeTLBEntry(i,new TranslationEntry(tlbBackup[i]));
			else	Machine.processor().writeTLBEntry(i,new TranslationEntry());
		}

//		printTLB();
		Machine.interrupt().restore(status);
	}
	@Override
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		memLock.acquire();
		
		byte[] memory = Machine.processor().getMemory();
		int amount=0,l=length,o=offset;
		do
		{
			VMKernel.handlePageFault(processID,vpn(vaddr),Lazy,coff);
			TranslationEntry e=findPage(vpn(vaddr));
			if (e==null || !e.valid)
			{
				memLock.release();
				return 0;
			}
			e.used=true;
			int inThisPage=Math.min(l, pageSize - offset(vaddr));
			System.arraycopy(memory, paddr(e.ppn,offset(vaddr)), data, o, inThisPage);
			l-=inThisPage;
			amount+=inThisPage;
			o+=inThisPage;
			vaddr=vaddr(vpn(vaddr)+1,0);
		}
		while (l>0);

		memLock.release();
		return amount;
	}
	@Override
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		memLock.acquire();

		byte[] memory = Machine.processor().getMemory();
		
		int amount=0,l=length,o=offset;
		do
		{
			VMKernel.handlePageFault(processID,vpn(vaddr),Lazy,coff);
			TranslationEntry e=findPage(vpn(vaddr));
			if (e==null || !e.valid)
			{
				memLock.release();
				return 0;
			}
			e.dirty=true;
			int inThisPage=Math.min(l, pageSize - offset(vaddr));
			System.arraycopy(data, o, memory, paddr(e.ppn,offset(vaddr)), inThisPage);
			l-=inThisPage;
			amount+=inThisPage;
			o+=inThisPage;
			vaddr=vaddr(vpn(vaddr)+1,0);
		}
		while (l>0);

		memLock.release();
		return amount;
	}
	
//	Deal with TLB
	protected int RoundRobin;
	protected void renewTLB(int vaddr)
	{
		memLock.acquire();
		int vpn=vpn(vaddr);
		VMKernel.handlePageFault(processID,vpn,Lazy,coff);
		RoundRobin=(RoundRobin+1)%tlbSize;
		for (int i=0;i<tlbSize;++i)
		if (!Machine.processor().readTLBEntry(i).valid)
		{
			RoundRobin=i;
			break;
		}
		TranslationEntry expired=Machine.processor().readTLBEntry(RoundRobin);
		if (expired.valid)
		{
			byte[] memory=Machine.processor().getMemory();
			TranslationEntry e=findPage(expired.vpn);
			e.dirty|=expired.dirty;
			e.used|=expired.used;
		}
		Machine.processor().writeTLBEntry(RoundRobin,new TranslationEntry(findPage(vpn)));
		memLock.release();
	}
	public void handleException(int cause) {
		Processor processor = Machine.processor();
		boolean status = Machine.interrupt().disable();
		switch (cause) {
		case Processor.exceptionTLBMiss:
			renewTLB(Machine.processor().readRegister(Processor.regBadVAddr));
			break;
		default:
			super.handleException(cause);
			break;
		}
		Machine.interrupt().restore(status);
	}


//	VM Process
	@Override
	protected boolean load(String name, String[] args)
	{
		memLock.acquire();
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");

		OpenFile executable = ThreadedKernel.fileSystem.open(name, false);
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			memLock.release();
			return false;
		}
		try {
			coff = new Coff(executable);
		} catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			memLock.release();
			return false;
		}

		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				memLock.release();
				return false;
			}
			if (!applyPages(section.isReadOnly(),numPages,section.getLength()))
			{
				cleanUp();
				memLock.release();
				return false;
			}
		}

		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 0;
		for (int i = 0; i < args.length; i++) {
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			coff.close();
			Lib.debug(dbgProcess, "\targuments too long");
			cleanUp();
			memLock.release();
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();

		// next comes the stack; stack pointer initially points to top of it
		if (!applyPages(false,numPages,stackPages))
		{
			cleanUp();
			memLock.release();
			return false;
		}
		
		initialSP = numPages * pageSize;

		// and finally reserve 1 page for arguments
		if (!applyPages(false,numPages,1))
		{
			cleanUp();
			memLock.release();
			return false;
		}

		if (!loadSections())
		{
			cleanUp();
			memLock.release();
			return false;
		}
		memLock.release();
		// store arguments in last page
		int entryOffset = (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;

		this.argc = args.length;
		this.argv = entryOffset;

		for (int i = 0; i < argv.length; i++) {
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
			Lib
					.assertTrue(writeVirtualMemory(entryOffset,
							stringOffsetBytes) == 4);
			entryOffset += 4;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset, argv[i]) == argv[i].length);
			stringOffset += argv[i].length;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset,
							new byte[] { 0 }) == 1);
			stringOffset += 1;
		}
		return true;
	}
	@Override
	protected boolean loadSections()
	{
		for (int s = 0; s < coff.getNumSections(); s++)
		{
			CoffSection section = coff.getSection(s);
	
			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");
	
			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;
				Lazy.put(vpn, new PII(s,i));
			}
		}
		return true;
	}
	protected void unloadSections() {
		super.unloadSections();
	}
	
	protected static final int pageSize = Processor.pageSize;
	protected static final char dbgProcess = 'a';
	protected static final char dbgVM = 'v';
}

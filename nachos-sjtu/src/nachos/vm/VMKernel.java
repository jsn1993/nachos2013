package nachos.vm;

import nachos.userprog.UserKernel;
import nachos.machine.*;

import java.util.*;

/**
 * A kernel that can support multiple demand-paging user processes.
 */
class PTI
{
	TranslationEntry first;
	int second;

	public PTI(TranslationEntry f,int s)
	{
		first=f;second=s;
	}
}
public class VMKernel extends UserKernel {
	/**
	 * Allocate a new VM kernel.
	 */
	private static int tlbSize;
//  Page Table
	private static HashMap<PII,TranslationEntry> memoryTable;
	private static HashMap<Integer,PTI> coremap;
	private static PTI pickMemoryVictim()
	{
		int t=Lib.random(coremap.size());
		for (Iterator<PTI> it=coremap.values().iterator();it.hasNext();)
		{
			PTI p=it.next();
			if ((t--)==0)
				return p;
		}
		return null;
	}
	public static TranslationEntry findPage(int pid,int vpn)
	{
		PII p=new PII(pid,vpn);
		if (!memoryTable.containsKey(p))
			return null;
		return memoryTable.get(p);
	}
	public static int takePage(int pid,int vpn,boolean readOnly)
	{
		int ppn=takePage();
		if (ppn<0)
		{
			PTI victim=pickMemoryVictim();
			moveToSwap(victim.second,victim.first.vpn);
			ppn=victim.first.ppn;
		}
		TranslationEntry e=memoryTable.get(new PII(pid,vpn));
		if (e==null)	e=new TranslationEntry(vpn,ppn,true,readOnly,false,false);
		else
		{
			e.vpn=vpn;
			e.ppn=ppn;
			e.valid=true;
			e.readOnly=readOnly;
			e.dirty=e.used=false;
		}
		memoryTable.put(new PII(pid,vpn),e);
		coremap.put(ppn,new PTI(e,pid));
		return ppn;
	}
	
	public static int resetEntry(int pid)
	{
		int ret=0;
		for (Iterator<Map.Entry<PII,TranslationEntry>> it=memoryTable.entrySet().iterator();it.hasNext();)
		{
			Map.Entry<PII,TranslationEntry> e=it.next();
			if (e.getKey().first==pid && e.getValue().valid)
			{
				returnPage(e.getValue().ppn);
				it.remove();
				coremap.remove(e.getValue().ppn);
				++ret;
			}
		}
		for (Iterator<Map.Entry<PII,Integer>> it=swapTable.entrySet().iterator();it.hasNext();)
		{
			Map.Entry<PII,Integer> e=it.next();
			if (e.getKey().first==pid)
			{
				collected.add(e.getValue());
				it.remove();
			}
		}
		return ret;
	}
	
//	Swap File
	private static HashMap<PII,Integer> swapTable;
	public static OpenFile Swap;
	private static LinkedList<Integer> collected;
	
	private static int allocSwap()
	{
		if (!collected.isEmpty())	return collected.poll();
		return swapTable.size();
	}
	public static void moveToSwap(int pid,int vpn)
	{
		PII p=new PII(pid,vpn);
		TranslationEntry e=memoryTable.get(p);
		//handle tlb
		for (int i=0;i<tlbSize;++i)
		{
			TranslationEntry tlb=Machine.processor().readTLBEntry(i);
			if (tlb.vpn==vpn && tlb.ppn==e.ppn)
			{
				e.dirty|=tlb.dirty;
				e.used|=tlb.used;
				if (memoryTable.get(p).dirty!=e.dirty || memoryTable.get(p).used!=e.used)
					System.out.println("link error");
				Machine.processor().writeTLBEntry(i,new TranslationEntry());
			}
		}
		e.valid=false;
		coremap.remove(e.ppn);
		TranslationEntry ee=memoryTable.get(p);
		if (ee.valid)
			System.out.println("Async");
		if (!swapTable.containsKey(p) || e.dirty)
		{
			if (!swapTable.containsKey(p))
				swapTable.put(p,allocSwap());
			Swap.write(swapTable.get(p)*pageSize,Machine.processor().getMemory(),e.ppn*pageSize,pageSize);
		}
	}
	public static void handlePageFault(int pid,int vpn,HashMap<Integer,PII> Lazy,Coff coff)
	{
		PII p=new PII(pid,vpn);
		if (memoryTable.containsKey(p) && memoryTable.get(p).valid)	return;
		boolean isLazy=Lazy.containsKey(vpn);
		boolean readOnly=false;
		if (isLazy)	readOnly=coff.getSection(Lazy.get(vpn).first).isReadOnly();
		int ppn=takePage(pid,vpn,readOnly);
		byte[] memory=Machine.processor().getMemory();
		TranslationEntry e=memoryTable.get(p);
		if (isLazy)
		{
			PII s=Lazy.remove(vpn);
			CoffSection section = coff.getSection(s.first);
			section.loadPage(s.second,ppn);
			e.dirty=e.used=true;
		}
		else
		if (swapTable.containsKey(p))
		{
			byte[] buf=new byte[pageSize];
			Swap.read(swapTable.get(p)*pageSize, buf, 0, pageSize);
			System.arraycopy(buf,0,Machine.processor().getMemory(),ppn*pageSize,pageSize);
			e.dirty=e.used=false;				
		}
		e.valid=true;
		e.ppn=ppn;
		coremap.put(ppn,new PTI(e,pid));
	}
	
	
//	VM Kernel
	public VMKernel() {
		super();
	}
	
	public void initialize(String[] args) {
		super.initialize(args); // get PageList, numPhysPages, offset started
		tlbSize=Machine.processor().getTLBSize();
		coremap=new HashMap<Integer,PTI>();
		Swap=Machine.stubFileSystem().open("SWAP", true);
		memoryTable=new HashMap<PII,TranslationEntry>();
		swapTable=new HashMap<PII,Integer>();
		collected=new LinkedList<Integer>();
	}
	
	public void selfTest() {
		super.selfTest();
	}
	
	public void run() {
		super.run();
	}

	public void terminate() {
		Machine.stubFileSystem().remove("SWAP");
		super.terminate();
	}
	
	private static final char dbgVM = 'v';
}
